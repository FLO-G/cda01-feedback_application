
CREATE DATABASE IF NOT EXISTS ray-pro;

USE ray-pro;

CREATE TABLE `role` (
  `id` integer PRIMARY KEY,
  `role` varchar(255)
);

CREATE TABLE `department` (
  `id` integer PRIMARY KEY,
  `name` varchar(255)
);

CREATE TABLE `user` (
  `id` integer PRIMARY KEY,
  `nom` varchar(255),
  `prenom` varchar(255),
  `mail` varchar(255),
  `mdp` varchar(255),
  `id_role` integer,
  `id_dept` integer
);

CREATE TABLE `participant` (
  `created_at` dateytime,
  `id_user` integer,
  `id_board` integer,
  PRIMARY KEY (`created_at`, `id_user`, `id_board`)
);

CREATE TABLE `board` (
  `id` integer PRIMARY KEY,
  `title` varchar(255),
  `description` varchar(255),
  `created_at` datetime,
  `id_user` integer
);

CREATE TABLE `feedback` (
  `id` integer PRIMARY KEY,
  `title` varchar(255),
  `description` varchar(255),
  `boolean` boolean,
  `id_board` integer
);

CREATE TABLE `resume` (
  `txt` text,
  `id_board` integer
);

ALTER TABLE `user` ADD FOREIGN KEY (`id`) REFERENCES `participant` (`id_user`);

ALTER TABLE `board` ADD FOREIGN KEY (`id`) REFERENCES `participant` (`id_board`);

ALTER TABLE `board` ADD FOREIGN KEY (`id`) REFERENCES `feedback` (`id_board`);

ALTER TABLE `board` ADD FOREIGN KEY (`id`) REFERENCES `resume` (`id_board`);

ALTER TABLE `role` ADD FOREIGN KEY (`id`) REFERENCES `user` (`id_role`);

ALTER TABLE `department` ADD FOREIGN KEY (`id`) REFERENCES `user` (`id_dept`);

ALTER TABLE `user` ADD FOREIGN KEY (`id`) REFERENCES `board` (`id_user`);
