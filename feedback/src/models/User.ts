// Utile pour notre component Vue
export type UserForm = {
  email: string,
  password: string,
  firstName: string
}

// Utile pour tout le reste de l'appli
export type User = {
  email: string,
  firstName: string
}