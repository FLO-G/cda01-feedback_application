import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/models/User'

export const useCurrentUserStore = defineStore('currentUser', () => {
  const user = ref<User>({
    email: 'store@mail.fr',
    firstName: 'Store'
  })

  function signin (login: string, password: string) {
    fetch('http://localhost/signin.php', {
      method: 'POST',
      body: {
        login,
        password
      } as any 
    })
    .then(response => {
      if(response.status === 200) {
      user.value.email = login
      user.value.firstName = login.split('@')[0]
      }
    })
  }

  return { user, signin }
})