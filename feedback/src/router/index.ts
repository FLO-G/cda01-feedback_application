import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/feedback',
      name: 'feedback-creation',
      component: () => import('../views/FeedbackCreationView.vue')
    },
    {
      path: '/retrospective',
      name: 'retrospective',
      component: () => import('../views/RetrospectiveCreationView.vue')
    },
    {
      path: '/signin',
      name: 'signin',
      component: () => import('../views/SigninSignupView.vue')
    },
  ]
})

export default router
